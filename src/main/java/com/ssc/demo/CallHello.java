package com.ssc.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CallHello {

    @Autowired
    private UserRemoteClient userRemoteClient;
    @GetMapping(value = "/callHello")
    public String calloHello(){
        //此时的调用相当于 http://eureka-user-provide/user/hello
        String result=userRemoteClient.hello();
        System.out.println("调用结果："+result);
        return result;
    }
}
