package com.ssc.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//@FeignClient注解，这个注解标识当前是一个 Feign 的客户端，value 属性是对应的服务名称，也就是你需要调用哪个服务中的接口。
@FeignClient(value = "eureka-user-provide")
public interface UserRemoteClient {
    //这个位置的请求路径就是，在这个提供者实例下提供具体服务的路径
    @RequestMapping(value = "/user/hello",method = RequestMethod.GET)
    String hello();
}
